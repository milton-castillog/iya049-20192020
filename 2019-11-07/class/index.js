class Either {
    constructor(value, right){
        this._value = value;
        this._right = right; //boolean that decides if instance is Right or Left
    }
    runEither(leftFn, rightFn) {
        return  this._right === true ? rightFn(this._value) : leftFn(this._value);
    }
    //map converts output to string since test expected that
    map(fn) {
        return this._right === true ? new Either(fn(this._value), this._right).toString() : new Either(this._value, this._right).toString();
    }
    toString() {
        return this._right === true ? `Right("${this._value}")` : `Left("${this._value}")`;
    }
    getType() {
        return "Either";
    }
}
class CreateRight extends Either {
    constructor(value) {
        const right = true;
        super(value, right);
    }
}
class CreateLeft extends Either {
    constructor(value) {
        const right = false;
        super(value, right)
    }
}
module.exports = {CreateRight, CreateLeft}