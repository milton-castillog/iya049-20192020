function Either (value, right) {
    this._value = value;
    this._right = right; //boolean that decides if instance is Right or Left
}
Either.prototype.runEither = function(leftFn, rightFn) {
    return this._right === true ? rightFn(this._value) : leftFn(this._value);
}
//map converts output to string since test expected that
Either.prototype.map = function(fn) {
    return this._right === true ? new Either(fn(this._value), this._right).toString() : new Either(this._value, this._right).toString();
}
Either.prototype.toString = function() {
    return this._right === true ? `Right("${this._value}")` : `Left("${this._value}")`;
}
Either.prototype.getType = function() {
    return "Either";
}
function CreateRight(value) {
    Either.call(this, value, true)
}
CreateRight.prototype = Object.create(Either.prototype);
function CreateLeft(value) {
    Either.call(this, value, false)
}
CreateLeft.prototype = Object.create(Either.prototype);
module.exports={CreateRight, CreateLeft}