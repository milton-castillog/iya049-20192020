//parameter right is a boolean that decides if instance is Right or Left
const either = (_value, _right)=> ({
    runEither: (leftFn, rightFn) => 
        _right === true ? rightFn(_value) : leftFn(_value),
    //map converts output to string since test expected that
    map: (fn) =>
        _right === true ? either(fn(_value), _right).toString() : either(_value, _right).toString(),
    toString: ()=>
        _right === true ? `Right("${_value}")` : `Left("${_value}")`,
    getType: () =>
        "Either",    
});

const createRight = (rightValue)=> either(rightValue, true);
const createLeft = (leftValue)=> either(leftValue, false);

module.exports={createRight, createLeft}