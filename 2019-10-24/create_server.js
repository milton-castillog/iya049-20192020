const events = require("events");
const net = require("net");

const createServer = () => {
  class MyEmitter extends events {};
  const myEmitter = new MyEmitter();

  let buffer = "";
  let onEvent = "";

  const server = net.createServer((socket) => {
    const response = {
      //writes the answer and close the socket 
      send: (info, headers, body)=>{
        let headers_str = ""
        for (const [key, value] of Object.entries(headers)) {
          headers_str += `${key}: ${value}\r\n`
        }
        const response_str =`HTTP/1.1 ${info}\r\n${headers_str}\r\n${body}\r\n`;
        //don't call the write function, because the end(data) do both, write(data) and then close the socket
        socket.end(response_str);
      }
    };

    socket.on("data", (data) => {
      buffer += data.toString("utf-8");
      //it checks the end of the data to start working with it 
      if(buffer.endsWith("\r\n\r\n")){
        const dataToParse = buffer.substr(0, buffer.length-4);
        buffer = "";
        //the base body of the request
        const request = {
          method: "",
          path: "",
          protocol: "",
          headers: {}
        };
        const splitedData = dataToParse.split("\r\n");
        const requestInfo1 = splitedData.shift().split(" ");
        //if requestInfo1.length < 3 throw error
        if(requestInfo1.length<3){
          throw "ERROR invalid size of request";
          
        }else{
          request.method = requestInfo1[0];
          request.path = requestInfo1[1];
          request.protocol = requestInfo1[2];
          //get all headers and parse them to go into request
          for (elem of splitedData){
            //if it reaches the end of of headers it just stops parsing
            if (elem==="")
              break;
            //split header since format is Accept: */*
            const headerPart = elem.split(": ");
            //use lowercase first part of the header as key and second part as value
            request.headers[headerPart[0].toLowerCase()] = headerPart[1];
          }
          myEmitter.emit(onEvent, request, response);
        }
      }
    }).on("error", (error)=>{
      console.log("ERROR", error)
      //server isn't closed nor the socket is ended because error event calls 'close' event on socket
    });
  });

  return {
    on: (event, handler) => {
      onEvent = event;
      myEmitter.on(onEvent, handler)
    },
    //it asigns the port number to the server
    listen: (port) => {
      server.listen(port)
    }
  };
};
module.exports = createServer;