const createServer = require("./create_server.js");
const server = createServer();

server.on("request", (request, response) => {
  console.log("received request", request);

  if (request.method !== "GET") {
    response.send(
      "404 Not Found",
      { "Content-Type": "text/plain" },
      "The server only supports HTTP method GET"
    );
    return;
  }

  switch (request.headers.accept) {
    case "application/json": {
      response.send(
        "200 OK",
        { "Content-Type": "application/json", "X-Powered-By": "Uneat" },
        JSON.stringify({
          success: true,
          from: request.path,
          message: "Hello World!"
        })
      );
      break;
    }
    default: {
      response.send(
        "200 OK",
        { "Content-Type": "text/plain", "X-Powered-By": "Uneat" },
        `Hello World from ${request.path}!`
      );
    }
  }
});

server.listen(8080);
