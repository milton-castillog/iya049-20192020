# Programación Web I - IYA049

Alumnos: Milton Castillo, Carlos Anuarbe, Efrain Salazar y Gustavo Olaya ✏️
Rama para las entregas: alumno/milton.castillo/master ✏️

## Instrucciones:

Encontrarás en el repositorio una rama con tu nombre ya creada. Utiliza el mismo formato para crear las ramas sucesivas. Anota la rama con tu nombre acabada en `master` en la línea 4 de este documento.

Ejemplo: si tu rama se llamara `gerardo-mh/admin`, para la práctica del día 13 de octubre crearías una rama con el nombre `gerardo-mh/practica-10-13`.
