let last_card = "none";
const card_types = ["ace-spades", "ace-diamonds"];

$(document).on('click',".unfound", (elem)=>{
    let element = $(elem.currentTarget)
    for (const card_name of card_types){
        if (element.hasClass(card_name)){
            element.children().attr("src","img/"+card_name+".png");
            checkLastCard(element, card_name);
            break;
        }
    }
});

function checkLastCard(current_card, name){
    if(last_card=="none")
        last_card = current_card;
    else if(last_card.hasClass(name) && current_card.attr('id') != last_card.attr('id')){
        last_card.removeClass("unfound");
        current_card.removeClass("unfound");
        last_card = "none";
        addToCounter();
    }
    else{
        setTimeout(()=>{
            last_card.children().attr("src","img/poker-card-back.png");
            current_card.children().attr("src","img/poker-card-back.png");
            last_card = "none";
        }, 300)
    }
}

function addToCounter(){
    if($("#game-container .unfound").length<=0)
        alert("¡Has ganado!");
}